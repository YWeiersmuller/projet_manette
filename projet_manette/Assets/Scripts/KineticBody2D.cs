﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KineticBody2D : MonoBehaviour {

    [SerializeField]
    Vector2 position;

    /*[SerializeField]
    float wallContactSpeedReduction;*/

    [SerializeField]
    float precision;
    Vector2 upperLeft;
    Vector2 upperRight;
    Vector2 lowerRight;
    Vector2 lowerLeft;

    bool groundTouch;
    bool rightTouch;
    bool leftTouch;
    bool topTouch;

    [SerializeField]
    float gravity;
    [SerializeField]
    float maxSpeed;
    [SerializeField]
    float inertia;
    [SerializeField]
    float jumpAccel;

    [SerializeField]
    //factor, which the base speed is multiplied with during dash;
    float dashSpeedmultiplier;
    [SerializeField]
    //minimum time between dash
    float dashCooldown;
    [SerializeField]
    //speed buff duration
    float dashDuration;

    //true if player is in mid-jump
    //used for double-jump / wall-jump
    bool jumping;
    bool secondJump;
    bool tryJump;
    public void TryJump()
    {
        this.tryJump = true;
    }
    private float accel;

    //movement given input for the frame
    private Vector2 speed;
    public Vector2 Speed
    {
        set
        {
            this.speed = value;
        }
        get
        {
            return this.speed;
        }
    }

    public void Jump()
    {
        this.accel = this.jumpAccel;
    }

    // Use this for initialization
	void Start ()
    {
        this.position = this.transform.position;
        this.upperLeft = new Vector2(transform.position.x - this.GetComponent<SpriteRenderer>().bounds.extents.x, transform.position.y + this.GetComponent<SpriteRenderer>().bounds.extents.y);
        this.upperRight = new Vector2(transform.position.x + this.GetComponent<SpriteRenderer>().bounds.extents.x, transform.position.y + this.GetComponent<SpriteRenderer>().bounds.extents.y);
        this.lowerRight= new Vector2(transform.position.x + this.GetComponent<SpriteRenderer>().bounds.extents.x, transform.position.y - this.GetComponent<SpriteRenderer>().bounds.extents.y);
        this.lowerLeft = new Vector2(transform.position.x - this.GetComponent<SpriteRenderer>().bounds.extents.x, transform.position.y - this.GetComponent<SpriteRenderer>().bounds.extents.y);
        this.speed = new Vector2(0, 0);
        accel = -gravity;
        /*llc = false;
        lrc = false;
        urc = false;
        ulc = false;*/
        groundTouch = false;
        leftTouch = false;
        rightTouch = false;
        topTouch = false;
        jumping = false;
        secondJump = false;
        tryJump = false;
    }
    

    void UpdatePositionData(Vector2 movement)
    {
        this.position += movement;
        this.upperLeft += movement;
        this.upperRight += movement;
        this.lowerLeft += movement;
        this.lowerRight += movement;
    }

    float CastRays(Vector2 movement)
    {
        //initials work like 'ULC = Upper Left (is in) Contact (with something)
        bool ulc=false;
        bool llc=false;
        bool lrc=false;
        bool urc=false;

        RaycastHit2D ulcRay = Physics2D.Raycast(this.upperLeft, movement);
        //if the corner is in direct contact : what is the nature of the contact ?
        if(ulcRay.distance == 0)
        {
            ulc = true;
            RaycastHit2D horizontalCheck = Physics2D.Raycast(this.upperLeft + new Vector2(precision, 0), movement);
            RaycastHit2D verticalCheck = Physics2D.Raycast(this.upperLeft + new Vector2(0, -precision), movement);
            if(horizontalCheck.distance == 0)
            {
                topTouch = true;
            }
            if(verticalCheck.distance == 0)
            {
                leftTouch = true;
            }
        }
        //Debug.Log(ulcRay.distance + " : ulcRay");


        RaycastHit2D urcRay = Physics2D.Raycast(this.upperRight, movement);
        if(urcRay.distance == 0)
        {
            urc = true;
            RaycastHit2D horizontalCheck = Physics2D.Raycast(this.upperRight + new Vector2(-precision, 0), movement);
            RaycastHit2D verticalCheck = Physics2D.Raycast(this.upperRight + new Vector2(0, -precision), movement);
            if (horizontalCheck.distance == 0)
            {
                topTouch = true;
            }
            if (verticalCheck.distance == 0)
            {
                rightTouch = true;
            }
        }
        //Debug.Log(urcRay.distance + " : urcRay");


        RaycastHit2D lrcRay = Physics2D.Raycast(this.lowerRight, movement);
        if (lrcRay.distance == 0)
        {
            lrc = true;
            RaycastHit2D horizontalCheck = Physics2D.Raycast(this.lowerRight + new Vector2(-precision, 0), movement);
            RaycastHit2D verticalCheck = Physics2D.Raycast(this.lowerRight + new Vector2(0, precision), movement);
            if (horizontalCheck.distance == 0)
            {
                groundTouch = true;
            }
            if (verticalCheck.distance == 0)
            {
                rightTouch = true;
            }
        }
        //Debug.Log(lrcRay.distance + " : lrcRay");


        RaycastHit2D llcRay = Physics2D.Raycast(this.lowerLeft, movement);
        if (llcRay.distance == 0)
        {
            llc = true;
            RaycastHit2D horizontalCheck = Physics2D.Raycast(this.lowerLeft + new Vector2(precision, 0), movement);
            RaycastHit2D verticalCheck = Physics2D.Raycast(this.lowerLeft + new Vector2(0, precision), movement);
            if (horizontalCheck.distance == 0)
            {
                groundTouch = true;
            }
            if (verticalCheck.distance == 0)
            {
                leftTouch = true;
            }
        }
        //Debug.Log(llcRay.distance + " : llcRay");

        float result = -1;

        if (!ulc)
        {
            result = ulcRay.distance;
        }

        if (!urc && result > urcRay.distance)
        {
            result = urcRay.distance;
        }
        if (!lrc && result > lrcRay.distance)
        {
            result = lrcRay.distance;
        }
        if (!llc && result > llcRay.distance)
        {
            result = llcRay.distance;
        }

        return result;
    }

    float GetDistanceFromVector2(Vector2 vect)
    {
        return Mathf.Sqrt((vect.x * vect.x) + (vect.y * vect.y));
    }

    // Update is called once per frame
    void Update ()
    {
        if (tryJump)
        {
            tryJump = false;
            if (!jumping)
            {
                jumping = true;
                Jump();
            }
            if(jumping && !secondJump)
            {
                secondJump = true;
                Jump();
            }
        }
        float delta = Time.deltaTime;
        Speed += new Vector2(0, accel) * delta;
        Vector2 movement = Speed * maxSpeed * delta;

        float distance = GetDistanceFromVector2(movement);

        //if we actually move
        if(distance != 0)
        {
            float closestObstacleDistance = CastRays(movement);

            //checking direct contacts first
            if (leftTouch)
            {
                if (jumping)
                {
                    movement.x += accel * delta;
                }
                Debug.Log("lefttouch");
                //movement.y = movement.y / wallContactSpeedReduction;
                if (movement.x < 0)
                { 
                    movement.x = 0;
                    speed.x = 0;
                }
            }
            if (rightTouch)
            {
                if (jumping)
                {
                    movement.x -= accel * Time.deltaTime;
                }
                Debug.Log("righttouch");
                //movement.y = movement.y / wallContactSpeedReduction;
                if (movement.x > 0)
                {
                    movement.x = 0;
                    speed.x = 0;
                }
            }
            if (groundTouch && movement.y < 0)
            {
                Debug.Log("groundtouch");
                Debug.Log(movement);
                movement.y = 0;
                speed.y = 0;
            }
            if (topTouch && movement.y > 0)
            {
                Debug.Log("toptouch");
                movement.y = 0;
                speed.y = 0;
            }
            
            //Debug.Log("Expected distance: " + distance + " VS closest object: " + closestObstacleDistance);
            if (closestObstacleDistance >= 0 && distance > closestObstacleDistance)
            {
                //got to resize the vect to not go through the obstacle
                movement = (closestObstacleDistance / distance) * movement;
                //if we collide because of a jump, reset jumping boolean
                if (jumping)
                {
                    jumping = false;
                }
                if (secondJump)
                {
                    secondJump = false;
                }
            }
            
            UpdatePositionData(movement);
            this.transform.position = this.position;
        }

        accel = -gravity;

        /*llc = false;
        lrc = false;
        urc = false;
        ulc = false;*/
        groundTouch = false;
        leftTouch = false;
        rightTouch = false;
        topTouch = false;
        //Debug.Log(accel);
        //Debug.Log(speed);
    }
}
