﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    private KineticBody2D body;



    // Use this for initialization
    void Start()
    {
        this.body = this.GetComponent<KineticBody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        Vector2 newspeed = new Vector2(Input.GetAxis("Horizontal"), body.Speed.y);
        body.Speed = newspeed;
        if (Input.GetButtonDown("A"))
        {
            this.body.TryJump();
        }

    }
}
